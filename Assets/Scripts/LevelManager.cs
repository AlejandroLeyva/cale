﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour
{
    private horizontalMovement cube;
    private horizontalMovementT tria;
    private horizontalMovementR rec;
    public GameObject currentCheckpointCube;
    public GameObject currentCheckpointTria;
    public GameObject currentCheckpointRec;

    public int vida;
    public GameObject vida1;
    public GameObject vida2;
    public GameObject vida3;
    public GameObject[] vidas;

    public GameObject camaraGO;
    // Start is called before the first frame update
    void Start()
    {
        cube = FindObjectOfType<horizontalMovement>();
        tria = FindObjectOfType<horizontalMovementT>();
        rec = FindObjectOfType<horizontalMovementR>();
        vida = GameControler.life;
        vidas = new GameObject[3];
        vidas[0]=vida1;
        vidas[1]=vida2;
        vidas[2]=vida3;

        switch (GameControler.life)
        {
            case 2:
                vidas[2].active = false;
                break;
            case 1:
                vidas[1].active = false;
                vidas[2].active = false;
                break;
            default:
                break;
        }
    }


    void Update()
    {
        Debug.Log("Vida Real" + vida);
    }

    public void respawnPlayerCube()
    {
        quitarVida();
        if (GameControler.life > 0)
        {
            Debug.Log("Player Respawn here");
            cube.transform.position = currentCheckpointCube.transform.position;
            cube.GetComponent<Rigidbody2D>().velocity = Vector3.zero;
        }
        else
        {
            gameOver();
        }
    }
    public void respawnPlayerTria()
    {
        quitarVida();
        if (GameControler.life > 0)
        {
            Debug.Log("Player Respawn here");
            tria.transform.position = currentCheckpointTria.transform.position;
            tria.GetComponent<Rigidbody2D>().velocity = Vector3.zero;
        }
        else
        {
            gameOver();
        }

    }
    public void respawnPlayerRec()
    {
        quitarVida();
        if (GameControler.life > 0)
        {
            Debug.Log("Player Respawn here");
            rec.transform.position = currentCheckpointRec.transform.position;
            rec.GetComponent<Rigidbody2D>().velocity = Vector3.zero;
        }
        else
        {
            gameOver();
        }

    }

    public void quitarVida()
    {
        GameControler.life--;
        vidas[GameControler.life ].active = false;
    }
    public void gameOver()
    {
        camaraGO.SetActive(true);

        foreach (GameObject o in Object.FindObjectsOfType<GameObject>())
        {
            if (o.gameObject.name != "Camera GameOver" && o.gameObject.name != "btnJugar" && o.gameObject.name != "Quad" && o.gameObject.name != "TextoGameOver")
                Destroy(o);
        }
    }
}
