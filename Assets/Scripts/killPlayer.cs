﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class killPlayer : MonoBehaviour
{
    
    public LevelManager levelmanager;

    public AudioClip sonidoMuerte;

    private Rigidbody2D rb;

    AudioSource fuenteAudio;
    // Start is called before the first frame update
    void Start()
    {
        levelmanager = FindObjectOfType<LevelManager>();
        fuenteAudio = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
       
    }

    void OnTriggerEnter2D(Collider2D other)
    {

        

        if (other.gameObject.tag == "PlayerCube")
        {
            sMuerte();
            //Poner pantalla de continuar
            levelmanager.respawnPlayerCube();
        }
        if (other.gameObject.tag == "PlayerTria")
        {
            sMuerte();
            //poner pantalla de continuar
            levelmanager.respawnPlayerTria();
        }
        if (other.gameObject.tag == "PlayerRec")
        {
            sMuerte();
            //poner pantalla de continuar
            levelmanager.respawnPlayerRec();
        }
        Debug.Log("vidas "+levelmanager.vida);
    }

    void sMuerte()
    {
        fuenteAudio.clip = sonidoMuerte;
        fuenteAudio.Play();
    }
}
