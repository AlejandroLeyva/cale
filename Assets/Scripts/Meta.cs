﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Meta : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject camaraGanaste;
    public GameObject principal;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void OnTriggerEnter2D(Collider2D other)
    {
        mostrarGanaste();
    }

    void mostrarGanaste()
    {
        camaraGanaste.SetActive(true);

        foreach (GameObject o in Object.FindObjectsOfType<GameObject>())
        {
            if (o.gameObject.name != "Camera Ganaste" && o.gameObject.name != "btnJugar" && o.gameObject.name != "Quad" && o.gameObject.name != "TextoGameOver" && o.gameObject.name != "Cup")
                Destroy(o);
        }
        principal.SetActive(false);
    }
}
