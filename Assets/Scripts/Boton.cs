﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boton : MonoBehaviour
{
    public GameObject puerta;
    private Rigidbody2D rb, rbp;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        rb.constraints = RigidbodyConstraints2D.FreezeAll;
        rbp = puerta.GetComponent<Rigidbody2D>();
        rbp.constraints = RigidbodyConstraints2D.FreezeAll;
        print("inicio boton");
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "PlayerTria" || collision.gameObject.tag == "PlayerCube" || collision.gameObject.tag == "PlayerRec")
        {
            Destroy(puerta.gameObject);
        }
        
    }
}
